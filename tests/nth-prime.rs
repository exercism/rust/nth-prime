#[cfg(test)]
mod tests {
    use nth_prime as prime;

    #[test]
    fn test_first_prime() {
        assert_eq!(prime::nth(0), 2);
    }

    #[test]
    fn test_second_prime() {
        assert_eq!(prime::nth(1), 3);
    }

    #[test]
    fn test_even_not_prime() {
        assert_ne!(prime::nth(2), 4);
    }

    #[test]
    fn test_seventh_prime() {
        assert_eq!(prime::nth(6), 17);
    }

    #[test]
    fn test_fifteenth_prime() {
        assert_eq!(prime::nth(14), 47);
    }

    #[test]
    fn test_one_hundred_ninety_third_prime() {
        assert_eq!(prime::nth(192), 1171);
    }

    #[test]
    fn test_nine_hundred_seventeenth_prime() {
        assert_eq!(prime::nth(916), 7177);
    }

    #[test]
    fn test_nine_hundred_seventy_ninth_prime() {
        assert_eq!(prime::nth(978), 7717);
    }

    #[test]
    #[ignore]
    fn test_large_prime() {
        assert_eq!(prime::nth(10_000), 104_743);
    }
}
