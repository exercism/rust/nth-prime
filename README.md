# Nth Prime

Given a number n, determine what the nth prime is.

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
the 6th prime is 13.

If your language provides methods in the standard library to deal with prime
numbers, pretend they don't exist and implement them yourself.

Remember that while people commonly count with 1-based indexing (i.e. "the 6th prime is 13"), many programming languages, including Rust, use 0-based indexing (i.e. `primes[5] == 13`). Use 0-based indexing for your implementation.

## Rust's useful functionalities
* [Naming](https://rust-lang-nursery.github.io/api-guidelines/naming.html)
| Item            | Convention             |
| --------------- | ---------------------- |
| Local variables | `snake_case`           |
| Functions       | `snake_case`           |
| Constants       | `SCREAMING_SNAKE_CASE` |

Examples:
```rust
let prime_first: u64 = 2;
```

```rust
fn sieve_eratosthenes(end: u64) -> Vec<u64> { ... }
```

```rust
const LIMIT_END: u64 = 4_294_967_295;
```

* [Loops](https://doc.rust-lang.org/book/ch03-05-control-flow.html#repetition-with-loops):
| Name  | Description |
| ----- | ----------- |
| For   | The **for loop** executes the code block for a specified number of times. It can be used to iterate over a fixed set of values, such as an array. The syntax of the for loop is as given below |
| While | The **while loop** executes the instructions each time the condition specified evaluates to true. |
| Loop  | The **loop** is a while(true) indefinite loop. |

Examples:
```rust
let mut number: u32 = 11;
let mut done: bool = false;

while !done {
    println!("{}", number);

    if number % 5 == 0 {
        // Multiple of 5
        done = true;
    }
    number += 1;
}
```

```rust
let mut number: u32 = 11;

loop {
    println!("{}", number);
    if number % 5 == 0 {
        // Multiple of 5
        break;
    }
    number += 1;
}
```
* [Iterators](https://doc.rust-lang.org/std/iter/trait.Iterator.html)
```rust

```

* Sequences
```rust
let start: u32 = 3;
let end: u32 = 60;

// Sequence of numbers from start to end (excluded)
let numbers = (start..end);

for number in numbers {
    println!("{}", number);
}
```

```rust
let start: u32 = 3;
let end: u32 = 60;
let step: usize = 2;

// Sequence of numbers from start to end step by step (excluded)
let odds = (start..end).step_by(step);

for odd in odds {
    println!("{}", odd);
}
```
[Iterator step by step](https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.step_by)

* [Tests organization](https://doc.rust-lang.org/stable/book/ch11-03-test-organization.html)


## Source

A variation on Problem 7 at Project Euler [http://projecteuler.net/problem=7](http://projecteuler.net/problem=7)