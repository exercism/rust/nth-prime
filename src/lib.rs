/// # Nth Prime
/// Given a position (n-1), 0-based indexing, determine what the nth prime is.
///
/// OBSERVATION: Use 0-based indexing, that means to access the nth prime must bu use the (n-1)th index
/// (i.e. 7th prime (17) is accessible by 6th index (primes[6]) like `primes[6] == 17`).
pub fn nth(position: usize) -> usize {
    let total: usize = position + 1;
    let primes: Vec<usize> = sieve_eratosthenes(total);

    // 0-indexed (n+1)th prime number
    primes[position]
}

/// Check candidate primality (based in before primes).
fn is_prime(candidate: usize, primes: &[usize]) -> bool {
    if candidate == 2 {
        return true;
    }

    primes.iter().all(|&prime| candidate % prime != 0)
}

/// The sieve of Eratosthenes
fn sieve_eratosthenes(total: usize) -> Vec<usize> {
    let mut primes: Vec<usize> = Vec::with_capacity(total);

    // The first prime and only even is 2
    primes.push(2);

    if total == 1 {
        return primes;
    }

    // Odds infinite sequence from 3
    let odds = (3..).step_by(2);

    for odd in odds {
        if is_prime(odd, &primes) {
            primes.push(odd);
        }

        if primes.len() == total {
            break;
        }
    }

    primes
}

#[cfg(test)]
mod tests {
    use super::is_prime;

    #[test]
    fn test_is_prime() {
        assert_eq!(is_prime(2, &[2]), true);
        assert_eq!(is_prime(3, &[2]), true);
        assert_eq!(
            is_prime(47, &[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43]),
            true
        );
    }

    #[test]
    fn test_is_not_prime() {
        assert_eq!(is_prime(4, &[2, 3]), false);
        assert_eq!(is_prime(9, &[2, 3, 5, 7]), false);
        assert_eq!(
            is_prime(
                69,
                &[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67]
            ),
            false
        );
    }
}
